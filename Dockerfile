# Creates an image with Oracle Java 8 and Hadoop 2.6.0 installed on Ubuntu

FROM ubuntu

MAINTAINER Amr Abed

USER root

RUN apt-get update
#ENV TZ America/Sao_Paulo
#RUN sed -i 'America/Sao_Paulo' /etc/timezone
#RUN dpkg-reconfigure -f noninteractive tzdata
#RUN apt-get install -y cron
#RUN /etc/init.d/cron stop
#RUN /etc/init.d/cron start

# Install Oracle java 8
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get update
RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
RUN echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
RUN apt-get install -y oracle-java8-installer 

# Install Hadoop 2.7.2
WORKDIR /usr/local
RUN apt-get install -y curl
RUN curl -s http://www.us.apache.org/dist/hadoop/common/current/hadoop-2.7.2.tar.gz | tar -xz
RUN mv hadoop-2.7.2 hadoop

# Install OpenSSH
RUN apt-get install -y openssh-server

# Enable SSH connection to localhost without passphrase
RUN ssh-keygen -t dsa -P '' -f ~/.ssh/id_dsa
RUN cat ~/.ssh/id_dsa.pub >> ~/.ssh/authorized_keys
RUN echo 'Host *\n  UserKnownHostsFile /dev/null\n  StrictHostKeyChecking no' > ~/.ssh/config


# Set environment variables
ENV PATH $PATH:/usr/local/hadoop/bin:/usr/local/hadoop/sbin
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV JAVA_OPTS -Xms500m -Xmx1000m -Dcom.sun.management.jmxremote
ENV HADOOP_HOME /usr/local/hadoop
ENV HADOOP_PREFIX /usr/local/hadoop
ENV HADOOP_OPTS -Djava.net.preferIPv4Stack=true
# Configure Hadoop for running a MapReduce job on YARN in a pseudo-distributed mode
WORKDIR /usr/local/hadoop
RUN sed -i '/^export JAVA_HOME/ s:.*:export JAVA_HOME=/usr/lib/jvm/java-8-oracle\nexport HADOOP_PREFIX=/usr/local/hadoop\n:' etc/hadoop/hadoop-env.sh
RUN sed -i '/<\/configuration>/i\\t<property>\n\t\t<name>fs.defaultFS<\/name>\n\t\t<value>hdfs://localhost:9000<\/value>\n\t</property>' etc/hadoop/core-site.xml
RUN sed -i '/<\/configuration>/i\\t<property>\n\t\t<name>dfs.replication</name>\n\t\t<value>1</value>\n\t</property>' etc/hadoop/hdfs-site.xml
RUN sed -i '/<\/configuration>/i\\t<property>\n\t\t<name>yarn.nodemanager.aux-services</name>\n\t\t<value>mapreduce_shuffle</value>\n\t</property>' etc/hadoop/yarn-site.xml
RUN sed '/<\/configuration>/i\\t<property>\n\t\t<name>mapreduce.framework.name</name>\n\t\t<value>yarn</value>\n\t</property>' etc/hadoop/mapred-site.xml.template > etc/hadoop/mapred-site.xml
#ADD hdfs-site.xml /usr/local/hadoop/etc/hadoop/    

RUN hdfs namenode -format
RUN service ssh start
RUN start-dfs.sh
RUN hdfs dfs -mkdir -p /user/root
RUN hdfs dfs -put etc/hadoop input

#INTALL ELASTICKSEARCH
WORKDIR /usr/local
RUN wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | apt-key add -
RUN echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list
RUN apt-get update && apt-get install elasticsearch
RUN sed -i '165,167d' /etc/init.d/elasticsearch


#INSTALL KIBANA
RUN echo "deb http://packages.elastic.co/kibana/4.4/debian stable main" | tee /etc/apt/sources.list.d/kibana.list
RUN apt-get update && apt-get -y install kibana
ADD kibana.yml /opt/kibana/config/
RUN apt-get install -y nginx apache2-utils
RUN htpasswd -c /etc/nginx/htpasswd.users kibana
ADD default /etc/nginx/sites-available/


EXPOSE 22 50070 9200 80
    
# By default, run a sample MapReduce job on YARN in a pseudo-distributed mode
CMD service ssh start && \
    start-dfs.sh && hdfs dfsadmin -safemode leave && \
    start-yarn.sh && service elasticsearch start && service kibana start && service nginx restart && bash 